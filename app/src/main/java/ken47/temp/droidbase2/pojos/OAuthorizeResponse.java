package ken47.temp.droidbase2.pojos;

/**
 * Created by kenster on 11/29/14.
 */
public class OAuthorizeResponse {
    private String accessToken;
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    public String getAccessToken() {
        return accessToken;
    }

    private Long expiresIn;
    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }
    public Long getExpiresIn() {
        return expiresIn;
    }
}
