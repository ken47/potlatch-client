package ken47.temp.droidbase2.models;

import com.google.common.base.Objects;

/**
 * Created by kenster on 11/27/14.
 */
public class GiftChain extends BaseEntity {
    private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private User user;
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(final Object _other) {
        GiftChain other = (GiftChain) _other;
        return hashCode() == other.hashCode();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getUser(), getName());
    }
}
