package ken47.temp.droidbase2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;

import ken47.temp.droidbase2.abstrakt.AuthorizedActivity;
import ken47.temp.droidbase2.helpers.CallableTask;
import ken47.temp.droidbase2.helpers.GiftApi;
import ken47.temp.droidbase2.helpers.SecuredRestBuilder;
import ken47.temp.droidbase2.helpers.TaskCallback;
import ken47.temp.droidbase2.models.Gift;
import retrofit.mime.TypedFile;

public class AddGiftActivity extends AuthorizedActivity {
    private SharedPreferences sharedPrefs;
    private EditText mTitleEdit, mCaptionEdit;
    private Button mTakePhotoButton;
    private Long chainId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setLayoutId(R.layout.add_gift_activity);
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            chainId = savedInstanceState.getLong("chain_id");
        } else {
            Bundle b = getIntent().getExtras();
            chainId = b.getLong("chain_id");
        }

        assert(chainId != null);

        sharedPrefs = getApplicationContext().getSharedPreferences(AuthorizedActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE);

        mTitleEdit = (EditText) findViewById(R.id.titleEditText);
        mCaptionEdit = (EditText) findViewById(R.id.captionEditText);
        mTakePhotoButton = (Button) findViewById(R.id.takePhotoButton);
    }

    protected void uploadPhoto() {
        CallableTask.invoke(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                GiftApi api = SecuredRestBuilder.getRestAdapter(sharedPrefs).create(GiftApi.class);
                api.uploadGiftImage(gift.getId(), new TypedFile("image/*", photoFile));
                return null;
            }
        }, new TaskCallback<Void>() {

            @Override
            public void success(Void whatever) {
                Toast.makeText(
                        AddGiftActivity.this,
                        "Photo load successful!",
                        Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(AddGiftActivity.this, SingleGiftActivity.class);
                Bundle b = new Bundle();
                b.putLong(SingleGiftActivity.GIFT_ID, gift.getId()); //Your id
                intent.putExtras(b);
                startActivity(intent);
            }

            @Override
            public void error(Exception e) {
                Log.e(AddGiftActivity.class.getName(), "Error loading gift.", e);

                Toast.makeText(
                        AddGiftActivity.this,
                        "Error uploading gift!",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {
            }
        });
    }

    private Gift gift;

    public void attemptToSave(View view) {
        CallableTask.invoke(new Callable<Gift>() {

            @Override
            public Gift call() throws Exception {
                GiftApi api = SecuredRestBuilder.getRestAdapter(sharedPrefs).create(GiftApi.class);
                Gift gift = new Gift();
                gift.setTitle(mTitleEdit.getText().toString());
                gift.setCaption(mCaptionEdit.getText().toString());
                return api.addGiftToChain(gift, chainId);
            }
        }, new TaskCallback<Gift>() {

            @Override
            public void success(Gift gift) {
                AddGiftActivity.this.gift = gift;

                // upload photo here
                uploadPhoto();
            }

            @Override
            public void error(Exception e) {
                Log.e(AddGiftActivity.class.getName(), "Error loading gift.", e);

                Toast.makeText(
                        AddGiftActivity.this,
                        "Load failed, check your Internet connection and credentials.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {
            }
        });
    }

    static final int REQUEST_TAKE_PHOTO = 1;
    private String filename;
    private File photoFile = null;

    public void takePhoto(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            photoFile = null;
            try {
                photoFile = createImageFile();
                filename = photoFile.getAbsolutePath();
            } catch (IOException ex) {
                // Error occurred while creating the File
                // TOAST HERE
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mTakePhotoButton.setText("Photo Taken");
        mTakePhotoButton.setEnabled(false);

        Toast.makeText(
                AddGiftActivity.this,
                filename,
                Toast.LENGTH_SHORT).show();
    }

    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }
}
