package ken47.temp.droidbase2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import ken47.temp.droidbase2.abstrakt.AuthorizedActivity;
import ken47.temp.droidbase2.helpers.CallableTask;
import ken47.temp.droidbase2.helpers.GiftApi;
import ken47.temp.droidbase2.helpers.SecuredRestBuilder;
import ken47.temp.droidbase2.helpers.TaskCallback;
import ken47.temp.droidbase2.helpers.UserState;
import ken47.temp.droidbase2.models.Gift;
import ken47.temp.droidbase2.models.User;

public class TopUsersActivity extends AuthorizedActivity {
    private SharedPreferences sharedPrefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setLayoutId(R.layout.top_users_activity);
        super.onCreate(savedInstanceState);
        sharedPrefs = getApplicationContext().getSharedPreferences(AuthorizedActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE);
        loadTopUsers();
    }

    private void displayUsers(List<User> users) {
        // We get the ListView component from the layout
        ListView lv = (ListView) findViewById(R.id.listView);
        final List<Map<String, String>> sourceList = new ArrayList<Map<String, String>>();

        int rank = 1;
        for(User user: users) {
            HashMap<String, String> tmp = new HashMap<String, String>();
            tmp.put("id", "" + user.getId());
            tmp.put("usernameWithRank", "" + rank + ") " + user.getUsername());
            tmp.put("touchedCount", "Touched Count: " + user.getTouchedCount());

            rank++;

            sourceList.add(tmp);
        }

        // This is a simple adapter that accepts as parameter
        // Context
        // Data list
        // The row layout that is used during the row creation
        // The keys used to retrieve the data
        // The View id used to show the data. The key number and the view id must match
        SimpleAdapter simpleAdapter = new SimpleAdapter(this, sourceList, R.layout.top_user_item_view, new String[] {"usernameWithRank", "touchedCount"}, new int[] {R.id.usernameText, R.id.touchedCountText });
        lv.setAdapter(simpleAdapter);
    }

    private void loadTopUsers() {
        CallableTask.invoke(new Callable<List<User>>() {
            @Override
            public List<User> call() throws Exception {
                GiftApi api = SecuredRestBuilder.getRestAdapter(sharedPrefs).create(GiftApi.class);

                return api.getTopUsers();
            }
        }, new TaskCallback<List<User>>()  {

            @Override
            public void success (List<User> users) {
                displayUsers(users);
            }

            @Override
            public void error (Exception e){
                Log.e(LoginActivity.class.getName(), "Error loading user info.", e);

                Toast.makeText(
                    TopUsersActivity.this,
                    "Load error! Try again in a minute.",
                    Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always () {}
        });

    }
}
