package ken47.temp.droidbase2.abstrakt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import ken47.temp.droidbase2.LoginActivity;
import ken47.temp.droidbase2.helpers.UserState;

public abstract class AuthorizedActivity extends DrawerActivity {
    public static final String APP_SHARED_PREFS = "KEN47_BASE";

    private void redirectIfLoggedOut() {
        SharedPreferences sharedPrefs = getApplicationContext().getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
        if (!UserState.isProbablyLoggedIn(sharedPrefs)) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        redirectIfLoggedOut();
        super.onResume();
    }

    // see State Machine: http://developer.android.com/training/basics/activity-lifecycle/starting.html
    // see http://stackoverflow.com/questions/17915393/android-login-activity-and-home-activity-redirection
    // ! explicitly excluding onCreate override from stackoverflow tip, for experimentation
    @Override
    protected void onRestart() {
        redirectIfLoggedOut();
        super.onRestart();
    }

    @Override
    protected void onStart() {
        redirectIfLoggedOut();
        super.onStart();
    }
}
