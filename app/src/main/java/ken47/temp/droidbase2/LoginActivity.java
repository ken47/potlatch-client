package ken47.temp.droidbase2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.Callable;

import ken47.temp.droidbase2.abstrakt.AuthorizedActivity;
import ken47.temp.droidbase2.abstrakt.UnAuthorizedActivity;
import ken47.temp.droidbase2.helpers.CallableTask;
import ken47.temp.droidbase2.helpers.GiftApi;
import ken47.temp.droidbase2.helpers.SecuredRestBuilder;
import ken47.temp.droidbase2.helpers.TaskCallback;
import ken47.temp.droidbase2.helpers.UserState;
import ken47.temp.droidbase2.models.User;
import ken47.temp.droidbase2.pojos.OAuthorizeResponse;

public class LoginActivity extends UnAuthorizedActivity {
    // todo: this would be cleaner in a config file somewhere
    public static final String CLIENT_ID = "web";
    public static final String CLIENT_SECRET = "secret";
    public static final String GRANT_TYPE = "password";

    private SharedPreferences sharedPrefs;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        sharedPrefs = getApplicationContext().getSharedPreferences(AuthorizedActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE);
        setContentView(R.layout.login);
    }

    public void attemptLogin(View view) {
        final String username = ((EditText)findViewById(R.id.loginUsername)).getText().toString();
        final String password = ((EditText)findViewById(R.id.loginPassword)).getText().toString();
        final Long now = System.currentTimeMillis() / 1000L;

        CallableTask.invoke(new Callable<String>() {

            @Override
            public String call() throws Exception {
                GiftApi loginApi = SecuredRestBuilder.getRestAdapterForLogin().create(GiftApi.class);
                OAuthorizeResponse response = loginApi.oauthAuthorize(
                        username,
                        password,
                        CLIENT_ID,
                        CLIENT_SECRET,
                        GRANT_TYPE
                );

                Log.d(LoginActivity.class.getCanonicalName(), "" + response.getAccessToken());

                // TODO: this APP_SHARED_PREFS thing def belongs in its own config
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(UserState.OAUTH_TOKEN, response.getAccessToken());
                editor.putLong(UserState.OAUTH_TOKEN_EXPIRES_AT, now + response.getExpiresIn());
                editor.commit();

                return response.getAccessToken();
            }
        }, new TaskCallback<String>() {

            @Override
            public void success(String result) {
                // OAuth 2.0 grant was successful and we
                // can talk to the server, open up the video listing
                getUserInfo();
            }

            @Override
            public void error(Exception e) {
                Log.e(LoginActivity.class.getName(), "Error logging in via OAuth.", e);

                Toast.makeText(
                        LoginActivity.this,
                        "Login failed, check your Internet connection and credentials.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {}
        });
    }

    public void getUserInfo() {
        CallableTask.invoke(new Callable<User>() {

            @Override
            public User call() throws Exception {
                GiftApi loginApi = SecuredRestBuilder.getRestAdapter(sharedPrefs).create(GiftApi.class);
                return loginApi.getUserInfo();
            }
        }, new TaskCallback<User>() {

            @Override
            public void success(User user) {
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(UserState.USERNAME, user.getUsername());
                editor.putString(UserState.EMAIL, user.getEmail());
                editor.putInt(UserState.UPDATE_INTERVAL, user.getUpdateInterval());
                editor.putBoolean(UserState.SHOULD_HIDE_FLAGGED, user.getShouldHideFlagged());
                editor.commit();

                // OAuth 2.0 grant was successful and we
                // can talk to the server, open up the video listing
                startActivity(new Intent(
                        LoginActivity.this,
                        GiftListActivity.class));
            }

            @Override
            public void error(Exception e) {
                Log.e(LoginActivity.class.getName(), "Error loading user info.", e);
                logout();
            }

            @Override
            public void always() {}
        });
    }

    public void logout() {
        CallableTask.invoke(new Callable<Boolean>() {

            @Override
            public Boolean call() throws Exception {
                GiftApi api = SecuredRestBuilder.getRestAdapter(sharedPrefs).create(GiftApi.class);
                Boolean response = api.logout();
                return response;
            }
        }, new TaskCallback<Boolean>() {

            @Override
            public void success(Boolean result) {
                // OAuth 2.0 grant was successful and we
                // can talk to the server, open up the video listing
                startActivity(new Intent(LoginActivity.this, GiftListActivity.class));
            }

            @Override
            public void error(Exception e) {
                Log.e(NavigationDrawerFragment.class.getName(), "Error logging out.", e);
                Log.d(NavigationDrawerFragment.class.getName(), e.getMessage());

                Toast.makeText(
                        LoginActivity.this,
                        "Login failed, check your Internet connection and credentials.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {
                SharedPreferences sharedPrefs = getSharedPreferences(AuthorizedActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(UserState.OAUTH_TOKEN, "");
                editor.putLong(UserState.OAUTH_TOKEN_EXPIRES_AT, 0);
                editor.commit();
            }
        });
    }

    public void goToRegister(View view) {
        startActivity(new Intent(
                LoginActivity.this,
                RegisterActivity.class));
    }
}
