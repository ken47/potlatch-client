package ken47.temp.droidbase2.helpers;

public interface TaskCallback<T> {

    public void success(T result);

    public void error(Exception e);

    public void always();
}
