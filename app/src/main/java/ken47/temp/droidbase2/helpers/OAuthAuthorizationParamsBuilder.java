package ken47.temp.droidbase2.helpers;

import ken47.temp.droidbase2.pojos.OAuthAuthorizationParams;

public class OAuthAuthorizationParamsBuilder {
    private String username;
    private String password;
    private String grantType;
    private String clientId;
    private String clientSecret;

    public OAuthAuthorizationParamsBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public OAuthAuthorizationParamsBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public OAuthAuthorizationParamsBuilder setGrantType(String grantType) {
        this.grantType = grantType;
        return this;
    }

    public OAuthAuthorizationParamsBuilder setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public OAuthAuthorizationParamsBuilder setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
        return this;
    }

    public OAuthAuthorizationParams build() {
        OAuthAuthorizationParams params = new OAuthAuthorizationParams();
        params.setUsername(username);
        params.setPassword(password);
        params.setClientId(clientId);
        params.setClientSecret(clientSecret);
        params.setGrantType(grantType);
        return params;
    }
}
