package ken47.temp.droidbase2.helpers;

/**
 * Created by kenster on 11/29/14.
 */

import android.content.SharedPreferences;

import com.google.common.io.BaseEncoding;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.internal.bind.DateTypeAdapter;

import org.apache.commons.io.IOUtils;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;

import retrofit.Endpoint;
import retrofit.ErrorHandler;
import retrofit.Profiler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.Log;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;
import retrofit.client.Client;
import retrofit.client.Client.Provider;
import retrofit.client.Header;
import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;
import retrofit.mime.FormUrlEncodedTypedOutput;

/**
 * A Builder class for a Retrofit REST Adapter. Extends the default implementation by providing logic to
 * handle an OAuth 2.0 password grant login flow. The RestAdapter that it produces uses an interceptor
 * to automatically obtain a bearer token from the authorization server and insert it into all client
 * requests.
 *
 * You can use it like this:
 *
 private VideoSvcApi videoService = new SecuredRestBuilder()
 .setLoginEndpoint(TEST_URL + VideoSvcApi.TOKEN_PATH)
 .setUsername(USERNAME)
 .setPassword(PASSWORD)
 .setClientId(CLIENT_ID)
 .setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
 .setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL).build()
 .create(VideoSvcApi.class);
 *
 * @author Jules, Mitchell
 *
 */
public class SecuredRestBuilder extends RestAdapter.Builder {
    // NOTE: SPRING SERVER
    public final static String SERVER = "http://192.168.56.1:8080";
    // NOTE: THIS USES NODEJS TO SERVE STATIC RESOURCES
    public final static String STATIC_SERVER = "http://192.168.56.1:3000";
    public final static String CLIENT_ID = "web";
    public final static String CLIENT_SECRET = "secret";

    public static RestAdapter getRestAdapterForLogin() {
        // converts camel-cased args to snake-case
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        return new SecuredRestBuilder()
                .setClient(new ApacheClient(new DefaultHttpClient()))
                .setEndpoint(SERVER).setLogLevel(LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build();
    }

    public static RestAdapter getRestAdapterForRegistration() {
        return new SecuredRestBuilder()
                .setClient(new ApacheClient(new DefaultHttpClient()))
                .setEndpoint(SERVER).setLogLevel(LogLevel.FULL)
                .build();
    }

    public static RestAdapter getRestAdapter(SharedPreferences sharedPreferences) {
        return new SecuredRestBuilder()
                .setSharedPreferences(sharedPreferences)
                .setClient(new ApacheClient(new DefaultHttpClient()))
                .setEndpoint(SERVER).setLogLevel(LogLevel.FULL)
                .build();
    }

    // this RequestInterceptor needs to be smart enough to refresh the token if it is expired
    private class OAuthHandler implements RequestInterceptor {
        public OAuthHandler() {
            super();
        }

        /**
         * Every time a method on the client interface is invoked, this method is
         * going to get called. The method checks if the client has previously obtained
         * an OAuth 2.0 bearer token. If not, the method obtains the bearer token by
         * sending a password grant request to the server.
         *
         * Once this method has obtained a bearer token, all future invocations will
         * automatically insert the bearer token as the "Authorization" header in
         * outgoing HTTP requests.
         *
         */
        @Override
        public void intercept(RequestFacade request) {
            if (sharedPreferences != null && UserState.isProbablyLoggedIn(sharedPreferences)) {
                request.addHeader("Authorization", "Bearer " + sharedPreferences.getString(UserState.OAUTH_TOKEN, ""));
            } else {
                // required to get a token
                String base64Auth = BaseEncoding.base64().encode(new String(CLIENT_ID + ":" + CLIENT_SECRET).getBytes());
                request.addHeader("Authorization", "Basic " + base64Auth);
            }
        }

    }

    private SharedPreferences sharedPreferences;
    private Client client;

    public SecuredRestBuilder setSharedPreferences(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        return this;
    }

    @Override
    public SecuredRestBuilder setEndpoint(String endpoint) {
        return (SecuredRestBuilder) super.setEndpoint(endpoint);
    }

    @Override
    public SecuredRestBuilder setEndpoint(Endpoint endpoint) {
        return (SecuredRestBuilder) super.setEndpoint(endpoint);
    }

    @Override
    public SecuredRestBuilder setClient(Client client) {
        this.client = client;
        return (SecuredRestBuilder) super.setClient(client);
    }

    @Override
    public SecuredRestBuilder setClient(Provider clientProvider) {
        client = clientProvider.get();
        return (SecuredRestBuilder) super.setClient(clientProvider);
    }

    @Override
    public SecuredRestBuilder setErrorHandler(ErrorHandler errorHandler) {

        return (SecuredRestBuilder) super.setErrorHandler(errorHandler);
    }

    @Override
    public SecuredRestBuilder setExecutors(Executor httpExecutor,
                                           Executor callbackExecutor) {

        return (SecuredRestBuilder) super.setExecutors(httpExecutor,
                callbackExecutor);
    }

    @Override
    public SecuredRestBuilder setRequestInterceptor(
            RequestInterceptor requestInterceptor) {

        return (SecuredRestBuilder) super
                .setRequestInterceptor(requestInterceptor);
    }

    @Override
    public SecuredRestBuilder setConverter(Converter converter) {

        return (SecuredRestBuilder) super.setConverter(converter);
    }

    @Override
    public SecuredRestBuilder setProfiler(@SuppressWarnings("rawtypes") Profiler profiler) {

        return (SecuredRestBuilder) super.setProfiler(profiler);
    }

    @Override
    public SecuredRestBuilder setLog(Log log) {

        return (SecuredRestBuilder) super.setLog(log);
    }

    @Override
    public SecuredRestBuilder setLogLevel(LogLevel logLevel) {

        return (SecuredRestBuilder) super.setLogLevel(logLevel);
    }


    @Override
    public RestAdapter build() {
        if (client == null) {
            client = new OkClient();
        }

        setRequestInterceptor(new OAuthHandler());

        return super.build();
    }
}
