package ken47.temp.droidbase2.helpers;

import java.util.Collection;
import java.util.List;

import ken47.temp.droidbase2.models.Gift;
import ken47.temp.droidbase2.models.GiftChain;
import ken47.temp.droidbase2.models.User;
import ken47.temp.droidbase2.pojos.OAuthorizeResponse;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;

/**
 * This interface defines an API for a VideoSvc. The
 * interface is used to provide a contract for client/server
 * interactions. The interface is annotated with Retrofit
 * annotations so that clients can automatically convert the
 *
 *
 * @author jules
 *
 */
public interface GiftApi {
    // this will be the only unprotected endpoint
    @POST("/register")
    public Boolean register(@Body User user);

    @GET("/gifts/recent")
    public Collection<Gift> getRecent();

    @GET("/users/{username}/gifts")
    public List<Gift> loadGiftsByUser(@Path("username") String username);

    @GET("/gifts/{id}")
    public Gift loadGift(@Path("id") Long id);

    @POST("/gifts/{id}/touch")
    public Gift touchGift(@Path("id") Long id);

    @POST("/gifts/{id}/untouch")
    public Gift unTouchGift(@Path("id") Long id);

    @POST("/gifts/{id}/flag")
    public Gift flagGift(@Path("id") Long id);

    @POST("/gifts/{id}/unflag")
    public Gift unFlagGift(@Path("id") Long id);

    @GET("/gifts/findByTitleLikeIgnoreCase/{title}")
    public List<Gift> searchGiftsByTitle(@Path("title") String title);

    @GET("/gifts/mostRecent")
    public List<Gift> getMostRecentGifts();

    @POST("/giftChains/{name}")
    public GiftChain createGiftChain(@Path("name") String name);

    @Multipart
    @POST("/gifts/{id}/upload")
    public Void uploadGiftImage(
            @Path("id") Long id,
            @Part("file") TypedFile file
    );

    @POST("/gifts/addToChain/{giftChainId}")
    public Gift addGiftToChain(@Body Gift gift, @Path("giftChainId") Long giftChainId);

    @GET("/users/getTopTwenty")
    public List<User> getTopUsers();

    @GET("/users/me")
    public User getUserInfo();

    @POST("/gifts")
    public Gift saveGift(@Body Gift gift);

    @POST("/users/me")
    public User persistSettings(@Body User user);

    @GET("/users/{username}/gifts")
    public Collection<Gift> getUserGifts(@Path("username") String username);

    @GET("/users/{username}/giftChains")
    public List<GiftChain> getUserGiftChains(@Path("username") String username);

    @GET("/goodbye")
    public Boolean logout();

    @FormUrlEncoded
    @POST("/oauth/token")
    public OAuthorizeResponse oauthAuthorize(
        @Field("username") String username,
        @Field("password") String password,
        @Field("client_id") String clientId,
        @Field("client_secret") String clientSecret,
        @Field("grant_type") String grantType
    );
}
