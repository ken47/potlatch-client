package ken47.temp.droidbase2.helpers;

/**
 * Created by kenster on 11/29/14.
 */
import java.util.concurrent.Callable;

import android.os.AsyncTask;
import android.util.Log;


/**
 * wrapper class for AsyncTask that can be swapped out with some other concurrency mechanism if needed
 *
 * @param <ParamType>
 * @param <ProgressType>
 * @param <ResultType>
 */
public class CallableTask<ParamType, ProgressType, ResultType> extends AsyncTask<ParamType, ProgressType, ResultType> {

    private static final String TAG = CallableTask.class.getName();

    public static <ParamType, ProgressType, ResultType> void invoke(Callable<ResultType> call, TaskCallback<ResultType> callback){
        new CallableTask<ParamType, ProgressType, ResultType>(call, callback).execute();
    }

    private Callable<ResultType> callable_;

    private TaskCallback<ResultType> callback_;

    private Exception error_;

    public CallableTask(Callable<ResultType> callable, TaskCallback<ResultType> callback) {
        callable_ = callable;
        callback_ = callback;
    }

    @Override
    protected ResultType doInBackground(ParamType... ts) {
        ResultType result = null;
        try{
            result = callable_.call();
        } catch (Exception e){
            Log.e(TAG, "Error invoking callable in AsyncTask callable: "+callable_, e);
            error_ = e;
        }
        return result;
    }

    @Override
    protected void onPostExecute(ResultType result) {
        if(error_ != null){
            callback_.error(error_);
        }
        else {
            callback_.success(result);
        }

        callback_.always();
    }
}
